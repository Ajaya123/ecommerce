const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');
 
// parse application/json
app.use(bodyParser.json());
 
//create database connection
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'ajaya123',
  database: 'product_details'
});
 
//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});
 
//show all productsList
app.get('/api/productsList',(req, res) => {
  let sql = "SELECT product_id,product_name FROM product";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
//show productdetails
app.get('/api/productsList/:id',(req, res) => {
    let sql = "SELECT * FROM product WHERE product_id="+req.params.id;
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": "product details", "error": null, "response": results}));
    });
    });
//add to cart
app.post('/api/addtocart',(req,res)=>{
    let data = {product_id:req.body.product_id,user_id:req.body.user_id,no_of_product:req.body.no_of_product};
    let sql="INSERT INTO cart SET ?";
    conn.query(sql,data,(err,results)=>{
        if(err){
            console.log(err);
            res.send(JSON.stringify({"messagr":"this product is not available", "error": null}));
        }
        else{
          res.send(JSON.stringify({"messagr":"product added to cart successfully", "error": null, "response": results}));
        }
    });
});
   //add new product
   app.post('/api/products',(req, res) => {
    let data = {product_name: req.body. product_name,product_information: req.body.product_information,
        price:req.body.price};
    let sql = "INSERT INTO product SET ?";
    let query = conn.query(sql, data,(err, results) => {
      if(err) throw err;
      res.send(JSON.stringify({"messagr":"successfully data inserted", "error": null, "response": results}));
    });
  });
  //view cart details
  app.get('/api/viewcart/:id',(req, res) => {
    
    let sql = "select user.user_id,name,product.product_id,product_name,cart.order_id,no_of_product,no_of_product*price as totalprice from cart INNER JOIN product ON cart.product_id=product.product_id INNER JOIN user ON cart.user_id=user.user_id and user.user_id="+req.params.id;
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
   
  });
  //
  app.get('/api/checkout/:userid',(req,res)=>{
    let sql = "select cart.user_id,user.name,sum(cart.no_of_product*product.price)as total from cart join product on(cart.product_id=product.product_id) join user on (cart.user_id=user.user_id) and user.user_id="+req.params.userid;
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
  });
  //create a new user
  app.post('/api/createuser',(req,res)=>{
    let data={name:req.body.name,email:req.body.email,password:req.body.password,number:req.body.number};
    let sql = "INSERT INTO user SET ?";
    let query = conn.query(sql, data,(err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"message":"user created", "error": null, "response": results}));
      });
});
app.get('/api/viewusers',(req,res)=>{
    let sql="SELECT * FROM user";
    let query=conn.query(sql,(err,results)=>{
        if(err) throw err;
        res.send(JSON.stringify({"message":"all user data","response":results}));
    });
});
app.delete('/api/delete/:order_id',(req,res)=>{
        conn.query("DELETE FROM cart WHERE order_id= ?",[req.params.order_id],(err,results)=>{
        if(err){

            res.send(JSON.stringify({"messagr":"record not found", "error": null}));

        }
        else{
            res.send(JSON.stringify({"messagr":"delete from cart successfully", "error": null, "response": results}));
        }
    });
});
app.listen(3000,() =>{
    console.log('Server started on port 3000...');
  });